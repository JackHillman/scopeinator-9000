export default (id, json) => ({
  type: 'IMPORT_NODE',
  payload: { id, json },
})
