export default (id, value) => ({
  type: 'CHANGE_VALUE',
  payload: { id, value },
})
