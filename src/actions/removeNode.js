export default (id) => ({
  type: 'REMOVE_NODE',
  payload: id,
})
