export default () => ({
  type: 'RESET_APP',
  payload: null,
})
