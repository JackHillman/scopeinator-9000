export default (id) => ({
  type: 'EXPORT_NODE',
  payload: id,
})
