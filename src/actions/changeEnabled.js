export default (id, value) => ({
  type: 'CHANGE_ENABLED',
  payload: { id, value: value },
})
