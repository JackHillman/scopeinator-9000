export default (id, value) => ({
  type: 'BLUR_VALUE',
  payload: { id, value },
})
