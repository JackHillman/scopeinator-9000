export default (id, title) => ({
  type: 'CHANGE_TITLE',
  payload: { id, title },
})
