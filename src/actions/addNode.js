import Node from '../engine/Node'

export default (node) => ({
  type: 'ADD_NODE',
  payload: new Node(node),
})
