import Node from '../engine/Node'
import Estimate from '../engine/Estimate'
import download from 'js-file-download'
import id from '../engine/id'

export const app = new Node({
  parent: null,
  title: 'App',
  id: 'root'
})

const nodes = new Map()

nodes.set(app.id, app)

export default (state = nodes, { type, payload }) => {
  let newState = new Map(state);
  let node, parent, subtree

  switch (type) {
    case 'ADD_NODE':
      parent = newState.get(payload.parent)
      parent.children.push(payload.id)

      newState.set(payload.id, payload)
      newState.set(payload.parent, parent)

      recalculateEstimates(newState.get(payload.id), newState)

      return newState
    case 'CHANGE_TITLE':
      node = newState.get(payload.id)

      node.title = payload.title
      newState.set(payload.id, node)

      return newState
    case 'CHANGE_VALUE':
      node = newState.get(payload.id)

      node.value = payload.value
      newState.set(payload.id, node)

      return newState
    case 'BLUR_VALUE':
      node = newState.get(payload.id)

      node.estimate = new Estimate(payload.value)
      node.value = node.estimate.value
      newState.set(payload.id, node)

      recalculateEstimates(node, newState)

      return newState
    case 'REMOVE_NODE':
      node = newState.get(payload)
      parent = newState.get(node.parent)

      parent.children = parent.children.filter(child => child !== payload)
      recalculateEstimates(node, newState)

      newState.set(parent.id, parent)

      deleteTree(node, newState)

      return newState
    case 'EXPORT_NODE':
      // Build the initial subtree
      subtree = buildSubtree(payload.id, new Map(), newState)
      // Rename the payload node to 'root'
      subtree.delete(payload.id)
      subtree.set('root', { ...newState.get(payload.id), id: 'root', parent: '' })
      subtree.get('root').children.forEach((id) => {
        const child = newState.get(id)
        child.parent = 'root'
        subtree.set(id, child)
      })

      download(JSON.stringify([...subtree], null, 2), `${subtree.get('root').title}.json`)

      return newState
    case 'IMPORT_NODE':
      // First build a new subtree
      subtree = payload.json.reduce((map, node) => {
        map.set(node[1].id, new Node(node[1]))

        return map
      }, new Map())

      // Then rebuild the ids to avoid conflicts
      let newRoot = null
      Array.from(subtree).forEach(([oldId, node]) => {
        const newNode = subtree.get(node.id)
        const newId = id()

        // Keep track of the imported tree root node
        if (newNode.id === 'root') {
          newRoot = newId
        }

        // Assign a new id to the node, and update the children
        newNode.id = newId
        newNode.children.forEach(id => {
          const child = subtree.get(id)
          child.parent = newId
          subtree.set(child.id, child)
        })

        // Remove the old reference, and add the new one
        subtree.delete(oldId)
        subtree.set(newId, newNode)

        // If the node has a parent, its children will need to be updated
        if (newNode.parent) {
          const parent = subtree.get(newNode.parent)
          parent.children = parent.children.map(id => id === oldId ? newId : id)
          subtree.set(parent.id, parent)
        }

        // Finally, push the new node
        newState.set(newId, newNode)
      })

      // Insert the imported root as a child of the payload node
      node = newState.get(payload.id)
      node.children.push(newRoot)
      newState.set(node.id, node)

      // Update the old root to have the payload node as a parent
      newRoot = newState.get(newRoot)
      newRoot.parent = payload.id
      newState.set(newRoot.id, newRoot)

      // Finally perform the estimate recalculations
      node.recalculateEstimate(node.children.map(id => newState.get(id)))
      recalculateEstimates(node, newState)

      return newState
    case 'CHANGE_ENABLED':
      node = newState.get(payload.id)
      node.enabled = payload.value
      newState.set(node.id, node)

      recalculateEstimates(node, newState)

      return newState
    case 'RESET_APP':
      return new Map(nodes)
    case 'EXPORT_APP':
      download(JSON.stringify([...newState], null, 2), `${newState.get('root').title}.json`)

      return newState
    case 'IMPORT_APP':
      return payload.reduce((map, node) => {
        map.set(node[1].id, new Node(node[1]))

        return map
      }, new Map())
    default:
      return newState
  }
}

const recalculateEstimates = (node, newState) => {
  while ((node = newState.get(node.parent)) !== undefined) {
    node.recalculateEstimate(node.children.map(id => newState.get(id)))
    newState.set(node.id, node)
  }
}

const deleteTree = (node, newState) => {
  newState.delete(node.id)
  node.children.forEach(child => deleteTree(newState.get(child), newState))
}

const buildSubtree = (id, map, newState) => {
  const parent = newState.get(id)

  map.set(id, parent) // Start with the current node
  parent.children.map(id => buildSubtree(id, map, newState)) // Recurse through tree

  return map
}
