import React, { Component } from 'react'
import { connect } from 'react-redux'
import { app } from '../../reducers/nodesReducer'
import NodeList from '../NodeList'
import Header from '../Header'
import styles from './index.module.scss'

class App extends Component {
  render = () => (
    <>
      <Header />
      <main className={styles.main}>
        <NodeList nodes={[app.id]}/>
      </main>
    </>
  )
}

export default connect(
  state => ({
    ...state
  })
)(App)
