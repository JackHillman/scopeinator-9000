import React, { Component } from 'react'
import styles from './index.module.scss'
import Node from '../Node'

class NodeList extends Component {
  static defaultProps = {
    nodes: [],
  }

  render() {
    return (
      <ul className={styles.list}>
        {this.props.nodes.map((node) => (
          <li key={node}>
            <Node id={node} />
          </li>
        ))}
      </ul>
    )
  }
}

export default NodeList
