import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import resetApp from '../../actions/resetApp'
import exportApp from '../../actions/exportApp'
import importApp from '../../actions/importApp'
import { FilePond } from 'react-filepond'
import 'filepond/dist/filepond.min.css'
import styles from './index.module.scss'
import './filepond.css'

class Header extends Component {
  resetApp = () => this.props.resetApp()

  exportApp = () => this.props.exportApp()

  importApp = (files) => {
    if (files.length >= 1) {
      const fileReader = new FileReader()

      fileReader.onload = (e) => {
        const json = JSON.parse(e.target.result)

        this.props.importApp(json)
        this.pond.removeFiles()
      }

      fileReader.readAsText(files[0].file)
    }
  }

  render() {
    return (
      <header className={styles.wrapper}>
        <ul className={styles.list}>
          <li className={[styles.listItem, styles.firstListItem].join(' ')}>
            <button onClick={this.resetApp.bind(this)}>Reset</button>
          </li>
          <li className={styles.listItem}>
            <button onClick={this.exportApp.bind(this)}>Export</button>
          </li>
          <li className={styles.listItem}>
            <FilePond labelIdle="Import" ref={ref => this.pond = ref} onupdatefiles={this.importApp.bind(this)}/>
          </li>
        </ul>
      </header>
    )
  }
}

export default connect(
  state => ({
    ...state
  }),
  dispatch => ({
    resetApp: bindActionCreators(resetApp, dispatch),
    exportApp: bindActionCreators(exportApp, dispatch),
    importApp: bindActionCreators(importApp, dispatch),
  })
)(Header)
