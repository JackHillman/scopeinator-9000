import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import swal from 'sweetalert'
import { FilePond } from 'react-filepond'
import addNode from '../../actions/addNode'
import changeTitle from '../../actions/changeTitle'
import changeValue from '../../actions/changeValue'
import blurValue from '../../actions/blurValue'
import removeNode from '../../actions/removeNode'
import exportNode from '../../actions/exportNode'
import importNode from '../../actions/importNode'
import changeEnabled from '../../actions/changeEnabled'
import styles from './index.module.scss'
import NodeList from '../NodeList'

class Node extends Component {
  constructor(props) {
    super(props)

    this.state = {
      focussed: false,
    }
  }

  componentDidMount() {
    if (this.node.new) {
      this.textInput.focus()
      this.node.new = false
    }
  }

  get node() {
    return this.props.nodes.get(this.props.id)
  }

  onTitleChange = (e) => {
    this.props.changeTitle(this.props.id, e.currentTarget.value)
  }

  onValueChange = (e) => {
    this.props.changeValue(this.props.id, e.currentTarget.value)
  }

  onBlurValue = (e) => {
    this.props.blurValue(this.props.id, e.currentTarget.value)
  }

  onEnabledChange = (e) => {
    this.props.changeEnabled(this.props.id, e.currentTarget.checked)
  }

  onAddChildClick = () => this.props.addNode({
    parent: this.node.id,
    value: this.node.children.length > 0 ? '' : this.node.value,
  })

  onDeleteClick = () => {
    if (this.node.children.length === 0) {
      return this.props.removeNode(this.props.id)
    }

    swal({
      title: 'Are you sure you want to delete this node?',
      text: `This node will be permanently destroyed and all of it's
             children will become orphans.

             (Not really, they will be destroyed too)`,
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    }).then(result => {
      if (result) {
        this.props.removeNode(this.props.id)
      }
    })
  }

  onExportClick = () => this.props.exportNode({
    id: this.node.id,
  })

  importNode = (files) => {
    if (files.length >= 1) {
      const fileReader = new FileReader()

      fileReader.onload = (e) => {
        const json = JSON.parse(e.target.result)

        this.props.importNode(this.node.id, json)
        this.pond.removeFiles()
      }

      fileReader.readAsText(files[0].file)
    }
  }

  get enabledCheckbox() {
    return (
      <input type="checkbox" checked={this.node.enabled} onChange={this.onEnabledChange.bind(this)} />
    )
  }

  get titleInput() {
    return (
      <input
        type="text"
        value={this.node.title}
        onChange={this.onTitleChange.bind(this)}
        ref={ref => this.textInput = ref}
        placeholder="New node"/>
    )
  }

  get estimateInput() {
    return (
      <input
        type="text"
        value={this.state.focussed ? this.node.value : this.node.estimate.value}
        onChange={this.onValueChange.bind(this)}
        onFocus={() => this.setState({ focussed: true })}
        onBlur={this.onBlurValue.bind(this)}
        placeholder="Time estimate"
        disabled={this.node.children.length > 0}/>
    )
  }

  get addChildButton() {
    return (
      <button onClick={this.onAddChildClick.bind(this)}>Add child</button>
    )
  }

  get deleteButton() {
    return this.node.parent === null ? null : (
      <button onClick={this.onDeleteClick.bind(this)}>Delete</button>
    )
  }

  get exportButton() {
    return (
      <button onClick={this.onExportClick.bind(this)}>Export</button>
    )
  }

  get filePond() {
    return (
      <FilePond labelIdle="Import" ref={ref => this.pond = ref} onupdatefiles={this.importNode.bind(this)}/>
    )
  }

  get body() {
    return <>
      {this.enabledCheckbox}
      {this.titleInput}
      {this.estimateInput}
      {this.addChildButton}
      {this.deleteButton}
      {this.exportButton}
      {this.filePond}
    </>
  }

  render = () => (
    <div className={this.node.enabled ? null : styles.disabled}>
      <div className={styles.body}>
        {this.body}
      </div>
      <NodeList nodes={this.node.children} />
    </div>
  )
}

export default connect(
  (store, ownProps) => ({
    nodes: store.nodes,
  }),
  dispatch => ({
    addNode: bindActionCreators(addNode, dispatch),
    changeTitle: bindActionCreators(changeTitle, dispatch),
    changeValue: bindActionCreators(changeValue, dispatch),
    blurValue: bindActionCreators(blurValue, dispatch),
    removeNode: bindActionCreators(removeNode, dispatch),
    exportNode: bindActionCreators(exportNode, dispatch),
    importNode: bindActionCreators(importNode, dispatch),
    changeEnabled: bindActionCreators(changeEnabled, dispatch),
  })
)(Node)
