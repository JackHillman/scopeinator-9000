import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'
import { createTransform, persistStore, persistReducer  } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import Node from './engine/Node'

const persistedReducer = persistReducer({
  key: 'root',
  stateReconciler: hardSet,
  storage,
  whitelist: ['nodes'],
  transforms: [
    createTransform(
      // transform state on its way to being serialized and persisted.
      (nodes) => {
        return [ ...nodes ]
      },
      // transform state being rehydrated
      (nodes) => {
        return nodes.reduce((map, node) => {
          map.set(node[1].id, new Node(node[1]))

          return map
        }, new Map())
      },
      { whitelist: ['nodes'] },
    ),
  ],
}, reducers)

const store = createStore(
  persistedReducer,
  applyMiddleware(thunk)
)

const persistor = persistStore(store)

export default () => ({ store, persistor })
