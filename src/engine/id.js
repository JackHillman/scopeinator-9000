import hash from 'object-hash'

export default () => hash(Date.now() + Math.random())
