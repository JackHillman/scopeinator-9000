import parse from 'parse-duration'

export const defaultUnit = 'h'
export const isOnlyNumber = value => `${parseFloat(value)}` === value
export const formatDuration = (value) => {
  if (value === 0) {
    return ''
  }

  return (value >= parse.hour) ? `${value / parse.hour}h` : `${value / parse.minute}m`
}

export const TYPE_SINGLE = 'TYPE_SINGLE'
export const TYPE_RANGE = 'TYPE_RANGE'

export default class Estimate {
  constructor(value) {
    this.stringValue = value
    this.valueType = TYPE_SINGLE
    this.value = value
  }

  get value() {
    return this.valueType === TYPE_SINGLE ? formatDuration(this._value) : this._value.map(formatDuration).join(' - ')
  }

  set value(value) {
    this._value = this.parseDuration(value)
  }

  addEstimate = (estimate) => {
    let thisValues = Array.isArray(this._value) ? this._value : [this._value]

    // If this is a single estimate, but the other is a range, convert this to a range
    if (this.valueType === TYPE_SINGLE && estimate.valueType === TYPE_RANGE) {
      thisValues.push(thisValues[0])
      this.valueType = TYPE_RANGE
    }

    if (estimate.valueType === TYPE_SINGLE) {
      // If the other estimate is just a single value, sum it to our value/s
      thisValues = thisValues.map(value => value += estimate._value)
    } else {
      // If the other estimate is a range, this must also be a range
      thisValues[0] += estimate._value[0]
      thisValues[1] += estimate._value[1]
    }

    this._value = this.valueType === TYPE_SINGLE ? thisValues[0] : thisValues

    return this
  }

  parseDuration = (value) => {
    if (`${value}`.includes('-')) {
      this.valueType = TYPE_RANGE

      return value.split('-').slice(0, 2).map(value => parse(
        isOnlyNumber(value) ? `${value}${defaultUnit}` : `${value}`
      )).sort((a, b) => a - b)
    }

    return parse(
      isOnlyNumber(value) ? `${value}${defaultUnit}` : `${value}`
    )
  }
}
