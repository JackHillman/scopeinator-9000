import id from './id'
import { store } from '../index'
import Estimate from './Estimate'

export default class Node {
  constructor(vars) {
    const fluent = {
      children: [],
      parent: '',
      title: '',
      id: id(),
      value: '',
      enabled: true,
      new: true,
      ...vars
    }

    for (let key in fluent) {
      this[key] = fluent[key]
    }

    this.estimate = new Estimate(this.value)
  }

  get childrenNodes() {
    const state = store.getState()

    return this.children.map(child => state.nodes.get(child))
  }

  recalculateEstimate = (children) => {
    this.estimate = children.reduce((estimate, node) => {
      if (node.enabled) {
        estimate.addEstimate(node.estimate)
      }

      return estimate
    }, new Estimate('0'))

    this.value = this.estimate.value
  }
}
