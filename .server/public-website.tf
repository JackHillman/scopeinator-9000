locals {
  origin_id = "${var.domain}-origin"
}

resource "aws_s3_bucket" "public-html" {
  bucket        = "${var.domain}"
  acl           = "public-read"
  force_destroy = true
  policy        = <<-POLICY
  {
    "Version":"2012-10-17",
    "Statement": [
      {
        "Sid":"PublicReadGetObject",
        "Effect":"Allow",
        "Principal": "*",
        "Action":["s3:GetObject"],
        "Resource":["arn:aws:s3:::${var.domain}/*"]
      }
    ]
  }
  POLICY

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    Environment = "${var.env}"
  }
}

resource "aws_kms_key" "logs-key" {
  description = "Access key for public website logs"
  deletion_window_in_days = 7

  tags = {
    Environment = "${var.env}"
  }
}

resource "aws_s3_bucket" "access-logs" {
  bucket = "${var.domain}-access-logs"
  acl = "private"
  force_destroy = true

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${aws_kms_key.logs-key.arn}"
        sse_algorithm = "aws:kms"
      }
    }
  }

  object_lock_configuration {
    object_lock_enabled = "Enabled"
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days = 60
      storage_class = "GLACIER"
    }
  }

  tags = {
    Environment = "${var.env}"
  }
}

resource "aws_cloudfront_distribution" "public-html-distribution" {
  origin {
    domain_name = "${aws_s3_bucket.public-html.bucket_regional_domain_name}"
    origin_id = "${local.origin_id}"
  }

  enabled = true
  is_ipv6_enabled = true
  default_root_object = "index.html"

  logging_config {
    include_cookies = false
    bucket = "${aws_s3_bucket.access-logs.bucket_domain_name}"
  }

  aliases = ["${var.domain}"]

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = "${local.origin_id}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    compress = true
  }

  tags = {
    Environment = "${var.env}"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = "${aws_acm_certificate.main-cert.arn}"
    ssl_support_method = "sni-only"
    minimum_protocol_version = "TLSv1.2_2018"
  }
}
