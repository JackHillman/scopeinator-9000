variable "main_zone_id" {
  type = string
}

variable "domain" {
  type = string
}

variable "env" {
  type = string
}
