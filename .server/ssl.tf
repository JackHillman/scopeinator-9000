resource "aws_acm_certificate" "main-cert" {
  provider          = "aws.us-east-1"
  domain_name       = "${var.domain}"
  validation_method = "DNS"

  tags = {
    Environment = "${var.env}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "main-cert-validation" {
  provider = "aws.us-east-1"
  name     = "${aws_acm_certificate.main-cert.domain_validation_options.0.resource_record_name}"
  type     = "${aws_acm_certificate.main-cert.domain_validation_options.0.resource_record_type}"
  zone_id  = "${data.aws_route53_zone.main.id}"
  records  = ["${aws_acm_certificate.main-cert.domain_validation_options.0.resource_record_value}"]
  ttl      = 60
}

resource "aws_acm_certificate_validation" "cert" {
  provider                = "aws.us-east-1"
  certificate_arn         = "${aws_acm_certificate.main-cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.main-cert-validation.fqdn}"]
}
