resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "dedicated"

  tags = {
    Environment = "${var.env}"
  }
}

resource "aws_subnet" "main-public" {
  availability_zone = "ap-southeast-2a"
  vpc_id            = "${aws_vpc.main.id}"
  cidr_block        = "10.0.32.0/19"

  tags = {
    Environment = "${var.env}"
  }
}

resource "aws_subnet" "main-private" {
  availability_zone = "ap-southeast-2a"
  vpc_id            = "${aws_vpc.main.id}"
  cidr_block        = "10.0.64.0/19"

  tags = {
    Environment = "${var.env}"
  }
}
