data "aws_route53_zone" "main" {
  zone_id      = "${var.main_zone_id}"
  private_zone = false

  tags = {
    Environment = "${var.env}"
  }
}

resource "aws_route53_record" "public-html" {
  zone_id = "${data.aws_route53_zone.main.zone_id}"
  name    = "${var.domain}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.public-html-distribution.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.public-html-distribution.hosted_zone_id}"
    evaluate_target_health = false
  }
}
